package com.sneaker.sneaker.bean;

public class Payment {
	
	private String refId;
	private CustomerCart cartId;
	private Double amount;
	public String getRefId() {
		return refId;
	}
	public void setRefId(String refId) {
		this.refId = refId;
	}
	public CustomerCart getCartId() {
		return cartId;
	}
	public void setCartId(CustomerCart cartId) {
		this.cartId = cartId;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	
}
