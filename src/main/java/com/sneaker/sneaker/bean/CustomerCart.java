package com.sneaker.sneaker.bean;

import java.util.List;

public class CustomerCart {
	private String cartId;
	private List<Sneaker> sneakerList;
	private Customer customerId;
	public String getCartId() {
		return cartId;
	}
	public void setCartId(String cartId) {
		this.cartId = cartId;
	}
	public List<Sneaker> getSneakerList() {
		return sneakerList;
	}
	public void setSneakerList(List<Sneaker> sneakerList) {
		this.sneakerList = sneakerList;
	}
	public Customer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Customer customerId) {
		this.customerId = customerId;
	}
	
	
}
