package com.sneaker.sneaker.bean;

public class SneakerSize {
	private String sizeId;
	private int sizeName;
	private int quantity;
	private Sneaker sneakerId;
	public String getSizeId() {
		return sizeId;
	}
	public void setSizeId(String sizeId) {
		this.sizeId = sizeId;
	}
	public int getSizeName() {
		return sizeName;
	}
	public void setSizeName(int sizeName) {
		this.sizeName = sizeName;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public Sneaker getSneakerId() {
		return sneakerId;
	}
	public void setSneakerId(Sneaker sneakerId) {
		this.sneakerId = sneakerId;
	}
	
	
}
