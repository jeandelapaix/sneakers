package com.sneaker.sneaker.bean;

import java.util.Date;

public class Sneaker {
	
	private String sneakerId;
	private String brandName;
	private String model;
	private Double price;
	private Date releaseDate;
	public String getSneakerId() {
		return sneakerId;
	}
	public void setSneakerId(String sneakerId) {
		this.sneakerId = sneakerId;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Date getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}
	
	
}
