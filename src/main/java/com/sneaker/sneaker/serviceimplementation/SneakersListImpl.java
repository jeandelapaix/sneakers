package com.sneaker.sneaker.serviceimplementation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.sneaker.sneaker.bean.Sneaker;
import com.sneaker.sneaker.service.SneakerServices;

@Service
public class SneakersListImpl implements SneakerServices {

	@Override
	public List<Sneaker> sneakersList() {
		// TODO Auto-generated method stub
		Sneaker s1 = new Sneaker();
		s1.setBrandName("Nike");
		s1.setModel("Airforce");
		s1.setPrice(20000.0);
		s1.setReleaseDate(new Date());
		s1.setSneakerId("1");
		Sneaker s2 = new Sneaker();
		s2.setBrandName("Nike");
		s2.setModel("AirMax");
		s2.setPrice(50000.0);
		s2.setReleaseDate(new Date());
		s2.setSneakerId("2");
		
		Sneaker s3 = new Sneaker();
		s3.setBrandName("Adidas");
		s3.setModel("Cortez");
		s3.setPrice(45000.0);
		s3.setReleaseDate(new Date());
		s3.setSneakerId("3");
		
		List<Sneaker> sneakers = new ArrayList<>();
		sneakers.add(s1);
		sneakers.add(s2);
		sneakers.add(s3);
		return sneakers;
	}

}
