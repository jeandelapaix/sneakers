package com.sneaker.sneaker.service;

import java.util.List;

import com.sneaker.sneaker.bean.Sneaker;

public interface SneakerServices {
	public List<Sneaker> sneakersList();

}
