package com.sneaker.sneaker.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.sneaker.sneaker.bean.Sneaker;
import com.sneaker.sneaker.service.SneakerServices;

@RestController
public class SneakerController {
	
	@Autowired
	private SneakerServices sneakerService;
	public ResponseEntity listSneaker() {
		List<Sneaker> sneakers = sneakerService.sneakersList();
		HashMap<String,Object> sneakHash = new HashMap<>();
		sneakHash.put("Sneakers", sneakers);
		return new ResponseEntity<>(sneakHash, HttpStatus.OK);
		
	}
}
